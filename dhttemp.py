# dthtemp.py
import time
import board
import adafruit_dht
class Dht11Temp:
    def __init__(self):
        # Initial the dht device, with data pin connected to:
        self.dhtDevice = adafruit_dht.DHT11(board.D18)
    def getTemp(self):
        try:
            # Print the values to the serial port
            temperature_c = self.dhtDevice.temperature
            temperature_f = temperature_c * (9 / 5) + 32
            humidity = self.dhtDevice.humidity
            return {'status':200, 'result': {'temperature_c': temperature_c, 'temperature_f': temperature_f, 'humidity':humidity}}
        except RuntimeError as error:
            # Errors happen fairly often, DHT's are hard to read, just keep going
            return {'status': 500, 'error': error.args[0] }
