# servo.py
from gpiozero import Servo
from gpiozero import Button
from time import sleep
class ServoControl:
    def __init__(self):
        self.servo = Servo(7)
        self.pos = 0 
    def leftservo(self):
        self.pos -= .25
        self.pos = self.pos if self.pos > -1 else -1
        self.servo.value = self.pos
        return {'servopos': self.pos}
    def rightservo(self):
        self.pos += .25
        self.pos = self.pos if self.pos < 1 else 1
        self.servo.value = self.pos
        return {'servopos': self.pos}
    def valservo(self):
        return {'servopos': self.pos}
