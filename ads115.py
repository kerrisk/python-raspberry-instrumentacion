# ads115.py
import board
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn

class AdsPot:
    def __init__(self):
        self.i2c = busio.I2C(board.SCL, board.SDA)
        self.ads = ADS.ADS1115(self.i2c)
    def getPotVal(self):
        chan0 = AnalogIn(self.ads, ADS.P0)
        return {'pot': {'voltage': chan0.voltage, 'value': chan0.value}}
