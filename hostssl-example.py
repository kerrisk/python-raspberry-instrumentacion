from werkzeug.wrappers import Request, Response
from werkzeug.serving import run_simple

from jsonrpc import JSONRPCResponseManager, dispatcher
from mpu import Mpu6050
from gpiobuttons import Gpiobuttons
from ads115 import AdsPot
from servo import ServoControl
from dhttemp import Dht11Temp
from ssl import SSLContext

mpu6050 = Mpu6050()
gpiobuttons = Gpiobuttons()
ads115 = AdsPot()
servo = ServoControl()
dhttemp = Dht11Temp()
sslcontext = SSLContext()
sslcontext.load_cert_chain('./fullchain.pem','./privkey.pem')
@dispatcher.add_method
def foobar(**kwargs):
    return kwargs["foo"] + kwargs["bar"]

def helloworld():
    return "Hello world"

@Request.application
def application(request):
    # Dispatcher is dictionary {<method_name>: callable}
    dispatcher["echo"] = lambda s: s
    dispatcher["add"] = lambda a, b: a + b
    dispatcher["hello"] = helloworld
    dispatcher["accelerometer"] = mpu6050.readMpuValues
    dispatcher["buttons"] = gpiobuttons.getStateButtons
    dispatcher["pot"] = ads115.getPotVal
    dispatcher["servoleft"] = servo.leftservo
    dispatcher["servoright"] = servo.rightservo
    dispatcher["servoval"] = servo.valservo
    dispatcher["dht11"] = dhttemp.getTemp
    response = JSONRPCResponseManager.handle(
        request.data, dispatcher)
    res = Response(response.json, mimetype='application/json')
    res.headers['Access-Control-Allow-Origin']='*'
    res.headers['Access-Control-Allow-Methods']='GET,PUT,POST,DELETE,PATCH'
    res.headers['Access-Control-Allow-Headers']='Content-Type, Authorization'
    #res.status_code = 500
    return res


if __name__ == '__main__':
    run_simple('0.0.0.0', 4000, application, ssl_context=sslcontext)