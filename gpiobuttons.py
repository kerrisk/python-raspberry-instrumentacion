# gpiobuttons.py
import RPi.GPIO as GPIO
import time
class Gpiobuttons:
    def __init__(self):
        self.btn1 = 12
        self.btn2 = 16
        GPIO.setup(self.btn1, GPIO.IN, GPIO.PUD_UP) 
        GPIO.setup(self.btn2, GPIO.IN, GPIO.PUD_UP) 
    def __del__(self):
        GPIO.cleanup()

    def getStateButtons(self):
            state = {'btn1': GPIO.input(self.btn1), 'btn2': GPIO.input(self.btn2)}
            return state
